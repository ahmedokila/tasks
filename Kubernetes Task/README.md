### Provision the Kubernetes Cluster with Minikube
```bash
minikube start --nodes=2 --profile=simple-cluster
```
![Alt text](Images/1.JPG)

### Restricting Access to Kubernetes API Server
We can use the network policy like
```bash
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: restrict-apiserver-access
  namespace: kube-system
spec:
  podSelector:
    matchLabels:
      component: kube-apiserver
  ingress:
    - from:
        - ipBlock:
            cidr: <specific Ip/32>
```

### Deployment for nginx ingress
```bash
minikube addons enable ingress --profile=simple-cluster
```
![Alt text](Images/2.JPG)
![Alt text](Images/3.JPG)

### Deployment of Juice Shop Application
```bash
kubectl create deployment juice-shop --image=bkimminich/juice-shop
kubectl get deployments
```
![Alt text](Images/4.0.JPG)

### Exposing the Juice Shop Application inside the Cluster
```bash
kubectl expose deployment juice-shop --port=3000
kubectl get services
minikube service  juice-shop --url --profile=simple-cluster
```
![Alt text](Images/4.1.JPG)
![Alt text](Images/5.JPG)
![Alt text](Images/6.JPG)

### Exposing the Juice Shop Application outside the Cluster using ingress
- #### Create the following ingress resource
```bash
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: example-ingress
      annotations:
        nginx.ingress.kubernetes.io/rewrite-target: /$1
    spec:
      rules:
        - host: localhost
          http:
            paths:
              - path: /
                pathType: Prefix
                backend:
                  service:
                    name: juice-shop
                    port:
                      number: 3000
```			  
- #### Apply the ingress resource inside the cluster
```bash
kubectl apply --filename=ingress.yml
kubectl get ingress
```
![Alt text](Images/7.JPG)

- #### Access the ingress
```bash
kubectl --namespace=ingress-nginx port-forward svc/ingress-nginx-controller 8085:80
```
![Alt text](Images/9.jpg)