import csv
import re


def is_valid_email(email):
    # Regular expression to validate email addresses
    pattern = r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
    return re.match(pattern, email)


def process_users(file_path):
    with open(file_path, mode="r") as file:
        reader = csv.reader(file)
        # Skip header of the CSV file
        next(reader)  
        for row in reader:
            if len(row) < 3:
                print("Warning: Invalid number of columns. Skipping this row.")
                continue
            name, email, id_number = row
            try:
                id_number = int(id_number)
                if id_number % 2 == 0:
                    id_type = "even"
                else:
                    id_type = "odd"
                if is_valid_email(email):
                    print(f"The ID of {email} is {id_type}.")
                else:
                    print(
                        f"Warning: Invalid email address for {name}. Skipping this user."
                    )
            except ValueError:
                print(f"Warning: Invalid ID number for {name}. Skipping this user.")
# Specify the path to your CSV file
file_path = "./data.csv"
process_users(file_path)
